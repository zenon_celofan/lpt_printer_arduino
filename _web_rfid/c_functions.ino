void print_web_page(void) {
  //send a standard http response header
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");  // the connection will be closed after completion of the response
  //client.println("Refresh: 5");  // refresh the page automatically every 5 sec
  client.println();
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");

//  client.println("<style>");
//  client.println("table, th, td {");
//  client.println("border: 1px solid black;");
//  client.println("padding: 5px;");
//  client.println("}");
//  client.println("table {");
//  client.println("border-spacing: 15px;");
//  client.println("}");
//  client.println("</style>");

  //page

  client.print("<b>RFID Cards access</b>");
  
  client.print("<form action=\"/\" >");    
  for (int i = 0; i < 10; i++) {
    client.print("<input type=\"checkbox\" name=\"card");
    client.print(i);
    client.print("\" value=\"1\" ");
    if (cards[i][4] == 1) {
      client.print("checked ");
    }
    client.print("> ");
      for (int j = 0; j < 4; j++) {
        client.print((int) cards[i][j], HEX);
        if (j != 3) {
          client.print('-');
        }
      }
    client.print("<br>");
   }

   client.print("<input type=\"submit\" value=\"Save\">");
   client.print("</form>");
   
  client.println("</html>");
}

int check_clearance(byte * tab) {
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 4; j++) {
      if (tab[j] != cards[i][j]) {
        break;
      }

      if (cards[i][4] == 1) {
        return CARD_OK; //card found and card ok
      } else {
        return CARD_NOK; //card found but not ok
      }
    }
  }
  return CARD_NOT_FOUND; //card not found
}
