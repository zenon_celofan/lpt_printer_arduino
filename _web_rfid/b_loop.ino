void loop() {
  //Check RFID readers status
  if (rfid.PICC_IsNewCardPresent() == true) {
    int card_status;
    rfid.PICC_ReadCardSerial();
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    for (int n = 0; n < 4; n++) {
      Serial.print(rfid.uid.uidByte[n], HEX);
      Serial.print(' ');
    }
    Serial.println();
    card_status = check_clearance(rfid.uid.uidByte);
    if (card_status == CARD_OK) {
      Serial.println("Card ok (printing message)");
      for (int i = 0; i < 7; i++) {
        printer_serial.print(printer_message[i]);
        Serial.print(printer_message[i]);
        delay(1000);
      }
      //delay(1000);
    } else if (card_status == CARD_NOK) Serial.println("Card not ok!");
    else if (card_status == CARD_NOT_FOUND) Serial.println("Card not found!");
    Serial.println();
  }

  // listen for incoming clients
  client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    readString = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();

        if (readString.length() < 100) {
          readString += c;
        }
        
        //Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          Serial.println(readString);
          if (readString.indexOf("GET /?") == 0 ) {
            if (readString.indexOf("card0") >0 ) cards[0][4] = 1;
            else cards[0][4] = 0;
            if (readString.indexOf("card1") >0 ) cards[1][4] = 1;
            else cards[1][4] = 0;
            if (readString.indexOf("card2") >0 ) cards[2][4] = 1;
            else cards[2][4] = 0;
            if (readString.indexOf("card3") >0 ) cards[3][4] = 1;
            else cards[3][4] = 0;
            if (readString.indexOf("card4") >0 ) cards[4][4] = 1;
            else cards[4][4] = 0;
            if (readString.indexOf("card5") >0 ) cards[5][4] = 1;
            else cards[5][4] = 0;
            if (readString.indexOf("card6") >0 ) cards[6][4] = 1;
            else cards[6][4] = 0;
            if (readString.indexOf("card7") >0 ) cards[7][4] = 1;
            else cards[7][4] = 0;
            if (readString.indexOf("card8") >0 ) cards[8][4] = 1;
            else cards[8][4] = 0;
            if (readString.indexOf("card9") >0 ) {
              cards[9][4] = 1;
              Serial.println("c-9 = 1");
            } else {
              cards[9][4] = 0;
              Serial.println("c-9 = 0");
            }
          }
          print_web_page();

          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}
