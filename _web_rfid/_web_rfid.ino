#include <SPI.h>
#include <MFRC522.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>


#define CARD_OK         1
#define CARD_NOK        0
#define CARD_NOT_FOUND  -1

String printer_message[] = {"public class SCRT PASSWD {\n",
                            "    public static print_me(String []args){\n",
                            "        for (int i = 1; i <= 9; i = i + 2) {\n",
                            "            System.out.print((char) ('a' + i));\n",
                            "        }\n",
                            "    }\n",
                            "}\n"};

//Software Serial
SoftwareSerial printer_serial(4, 3); // RX, TX

//RFID
int RFID_CS_PIN = 9;
int RFID_RST_PIN = 2;
MFRC522 rfid(RFID_CS_PIN, RFID_RST_PIN);

//Ethernet
byte mac[] = {0xc7, 0x7f, 0xea, 0x45, 0x81, 0x8b};
IPAddress ip(192, 168, 1, 177);
EthernetServer server(80);
EthernetClient client;
String readString;

//Registered Tags:
byte cards[10][5] = {       // 4bytes ID number, bool access_clearance
  {0xA0, 0x6C, 0xAE, 0x56, 1},
  {0xA3, 0xF8, 0x6E, 0x85, 0},
  {0x31, 0x1E, 0x4B, 0x7B, 0},
  {0x10, 0x58, 0x7E, 0xA6, 1},
  {0x05, 0x56, 0x2D, 0x83, 0},  //blue tag
  {0x04, 0x56, 0x4A, 0x7B, 0},
  {0x32, 0x35, 0x4A, 0x7B, 0},
  {0x0C, 0xE7, 0x5F, 0x43, 1},  //red tag
  {0x31, 0xEF, 0x4A, 0x7B, 0},
  {0xD0, 0xA9, 0x4A, 0x7B, 0},
};

//Functions declarations
int check_clearance(byte *);
void print_web_page(void);
