void setup() {
  digitalWrite(LED_BUILTIN, HIGH);        //RED led will be on during setup

  //Serial init
  Serial.begin(9600);
  printer_serial.begin(9600);

  //SPI
  Serial.print("SPI: ");
  SPI.begin();
  Serial.println("init OK");

  //RFID init
  Serial.print("RC522: ");
  rfid.PCD_Init();
  Serial.print("init OK - ");
  rfid.PCD_DumpVersionToSerial();

  //Ethernet
  Serial.print("Ethernet: ");
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  
  //Setup complete
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("Setup complete.");
}
