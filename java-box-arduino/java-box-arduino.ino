#define ON  0
#define OFF 1

int relay_pin = 19;
int gen_pin = 10;
int col_pin = 11;
int f_in[] = {2, 4, 6, 8};
int f_out[] = {3, 5, 7, 9};

int blue_pin = 14;   //A0
int green_pin = 16;  //A2
int red_pin = 15;    //A1



void setup() {
  pinMode(relay_pin, OUTPUT);
  digitalWrite(relay_pin, HIGH);

  pinMode(gen_pin, OUTPUT);
  digitalWrite(gen_pin, HIGH);

  pinMode(col_pin, INPUT_PULLUP);


  for (int i = 0; i < 4; ++i) {
    pinMode(f_in[i], INPUT_PULLUP);
    pinMode(f_out[i], OUTPUT);
    digitalWrite(f_out[i], HIGH);
  }
  
  pinMode(blue_pin, OUTPUT);
  digitalWrite(blue_pin, HIGH);

  pinMode(green_pin, OUTPUT);
  digitalWrite(green_pin, HIGH);

  pinMode(red_pin, OUTPUT);
  digitalWrite(red_pin, HIGH);
  
}

// the loop function runs over and over again forever
void loop() {

  blue_led(ON);
  if (check_connections() == true) {
    red_led(OFF);
    green_led(ON);
    release_lock();
  } else {
    red_led(ON);
    green_led(OFF);
    engage_lock();
  }
  blue_led(OFF);

  delay(1000);
}


bool check_connections(void) {
  int correct_bridges = 0;
  int bridge[5][2] = {{gen_pin, f_in[0]},
                    {f_out[0], f_in[1]},
                    {f_out[1], f_in[2]},
                    {f_out[2], f_in[3]},
                    {f_out[3], col_pin}};

  for(int b = 0; b < 5; ++b) {
    digitalWrite(bridge[b][0], LOW);
    delay(20);
    if (digitalRead(bridge[b][1]) == LOW) {
      correct_bridges++;
    }
    digitalWrite(bridge[b][0], HIGH);
    delay(20);
  }

  if (correct_bridges == 5) {
    return true;
  } else {
    return false;
  }
}


void release_lock(void) {
  digitalWrite(relay_pin, LOW);
}


void engage_lock(void) {
  digitalWrite(relay_pin, HIGH);
}


void blue_led(int state) {
  digitalWrite(blue_pin, state);
}


void green_led(int state) {
  digitalWrite(green_pin, state);
}


void red_led(int state) {
  digitalWrite(red_pin, state);
}
